# docker build -t registry.gitlab.com/rodrigodalri/url_shortener_project/3.9.13-alpine3.16 .
# docker push registry.gitlab.com/rodrigodalri/url_shortener_project/3.9.13-alpine3.16

FROM python:3.9.13-alpine3.16

WORKDIR /url_shortener_project

RUN apk update && apk upgrade
RUN apk add --no-cache bash \
                       build-base \
                       gcc \
                       libffi-dev \
                       linux-headers \
                       musl-dev \
                       pkgconfig \
                       python3-dev \
    && rm -rf /var/cache/apk/*

COPY ./requirements.txt /code/requirements.txt

RUN pip install --no-cache-dir --upgrade -r /code/requirements.txt

COPY ./ /url_shortener_project/

CMD ["uvicorn", "shortener_app.main:app", "--host", "0.0.0.0", "--port", "8080"]
