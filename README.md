# FastAPI URL Shortener

[![Latest Release](https://gitlab.com/rodrigodalri/url_shortener_project/-/badges/release.svg)](https://gitlab.com/rodrigodalri/url_shortener_project/-/releases) [![Pipeline status](https://gitlab.com/rodrigodalri/url_shortener_project/badges/main/pipeline.svg)](https://gitlab.com/rodrigodalri/url_shortener_project/-/commits/main) [![Coverage report](https://gitlab.com/rodrigodalri/url_shortener_project/badges/main/coverage.svg)](https://gitlab.com/rodrigodalri/url_shortener_project/-/commits/main)

Project from [Build a URL Shortener With FastAPI and Python (Real Python)](https://realpython.com/build-a-python-url-shortener-with-fastapi/)

## Install the Project

1. Create a Python virtual environment

```sh
$ python -m venv venv
$ source venv/bin/activate
(venv) $
```

2. Install the requirements

```
(venv) $ pip3 install -r requirements.txt
```

## Run the Project

You can run the project locally with this command in your terminal:

```sh
(venv) $ uvicorn shortener_app.main:app --reload
```

Your server will reload automatically when you change a file.

If you want to run in a docker container:

```sh
$ docker-compose up -d
```

## Visit the Documentation

When the project is running you can visit the documentation in your browser:

- http://127.0.0.1:8080/docs
- http://127.0.0.1:8080/redoc
