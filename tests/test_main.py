from fastapi.testclient import TestClient

from shortener_app.config import get_settings
from shortener_app.main import app

client = TestClient(app)

def test_read_root():
    response = client.get("/")

    assert response.status_code == 200
    assert response.json() == {"msg": "Welcome to the URL shortener API :)"}


def test_create_url():
    response = client.post("/url",  json={"target_url": "http://gitlab.com"})

    fake_response = {
        "target_url": "http://gitlab.com",
        "is_active": "True",
        "clicks": "0",
        "url": f"{get_settings().base_url}/K7R84",
        "admin_url": f"{get_settings().base_url}/admin/K7R84_7OH56XZX"
    }

    assert response.status_code == 200
    assert response.json().keys() == fake_response.keys()
    assert response.json().get("target_url") == fake_response.get("target_url")
    assert response.json().get("is_active") is True
    assert response.json().get("clicks") == 0
    assert response.json().get("url").startswith(get_settings().base_url)
    assert response.json().get("admin_url").startswith(f"{get_settings().base_url}/admin/")


def test_create_url_bad_payload():
    response = client.post("/url",  json={"url": "http://gitlab.com"})

    assert response.status_code == 422
    assert response.json().get("detail")[0].get("msg") == "field required"
    assert response.json().get("detail")[0].get("type") == "value_error.missing"


def test_create_url_format():
    response = client.post("/url",  json={"target_url": "gitlab.com"})

    assert response.status_code == 400
    assert response.json().get("detail") == "Your provided URL is not valid"


def test_create_url_without_payload():
    response = client.post("/url")

    assert response.status_code == 422
    assert response.json().get("detail")[0].get("msg") == "field required"
    assert response.json().get("detail")[0].get("type") == "value_error.missing"


def test_create_url_wrong_method():
    response = client.put("/url")

    assert response.status_code == 405
    assert response.json().get("detail") == "Method Not Allowed"


def test_forward_to_target_url():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    key = response.json().get("url").split(f"{get_settings().base_url}/")[1]

    response2 = client.get(f"/{key}")

    assert response2.status_code == 200
    assert response2.url == "http://gitlab.com/"
    assert response2.history[0].status_code == 307


def test_forward_to_inexistent_target_url():
    response = client.get("/00000")

    assert response.status_code == 404


def test_forward_to_target_url_wrong_method():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    key = response.json().get("url").split(f"{get_settings().base_url}/")[1]

    response2 = client.post(f"/{key}")

    assert response2.status_code == 405
    assert response2.json().get("detail") == "Method Not Allowed"


def test_get_url_info():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    secret_key = response.json().get("admin_url").split("/admin/")[1]

    response2 = client.get(f"/admin/{secret_key}")

    assert response2.status_code == 200
    assert response.json() == response2.json()


def test_get_url_info_inexistent_key():
    response = client.get("/admin/00000_00000000")

    assert response.status_code == 404


def test_get_url_info_wrong_method():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    secret_key = response.json().get("admin_url").split("/admin/")[1]

    response2 = client.patch(f"/admin/{secret_key}")

    assert response2.status_code == 405
    assert response2.json().get("detail") == "Method Not Allowed"


def test_delete_url():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    secret_key = response.json().get("admin_url").split("/admin/")[1]

    response2 = client.delete(f"/admin/{secret_key}")

    assert response2.status_code == 200
    assert response2.json().get("detail") == "Successfully deleted shortened URL for 'http://gitlab.com/'"


def test_delete_url_inexistent_key():
    response = client.delete("/admin/00000_00000000")

    assert response.status_code == 404
    assert response.json().get("detail") == "URL 'http://testserver/admin/00000_00000000' doesn't exist"


def test_delete_url_wrong_method():
    response = client.post("/url",  json={"target_url": "http://gitlab.com/"})
    secret_key = response.json().get("admin_url").split("/admin/")[1]

    response2 = client.post(f"/admin/{secret_key}")

    assert response2.status_code == 405
    assert response2.json().get("detail") == "Method Not Allowed"
