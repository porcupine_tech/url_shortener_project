from shortener_app.keygen import create_random_key, create_unique_random_key
from shortener_app.main import get_db


def test_create_random_key():
    random_keys = [create_random_key() for _ in range(1000)]

    assert len(set(random_keys)) == len(random_keys)


def test_create_unique_random_key():
    db = next(get_db())
    unique_keys = [create_unique_random_key(db=db) for _ in range(1000)]

    assert len(set(unique_keys)) == len(unique_keys)
